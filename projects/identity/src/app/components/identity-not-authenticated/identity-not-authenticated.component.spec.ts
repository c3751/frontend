import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IdentityNotAuthenticatedComponent } from './identity-not-authenticated.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('IdentityNotAuthenticatedComponent', () => {
  let component: IdentityNotAuthenticatedComponent;
  let fixture: ComponentFixture<IdentityNotAuthenticatedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [IdentityNotAuthenticatedComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IdentityNotAuthenticatedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
