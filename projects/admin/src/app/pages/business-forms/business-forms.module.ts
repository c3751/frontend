import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BusinessFormsRoutingModule } from './business-forms-routing.module';
import { BusinessFormsMainPageComponent } from './business-forms-main-page/business-forms-main-page.component';


@NgModule({
  declarations: [
    BusinessFormsMainPageComponent
  ],
  imports: [
    CommonModule,
    BusinessFormsRoutingModule
  ]
})
export class BusinessFormsModule { }
