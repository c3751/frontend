import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SignInCompleteComponent } from './sign-in-complete.component';

describe('SignInCompleteComponent', () => {
  let component: SignInCompleteComponent;
  let fixture: ComponentFixture<SignInCompleteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SignInCompleteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SignInCompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
