import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BackendModule } from '@kazgis/backend/backend.module';
import { AuthModule } from '@kazgis/core/auth/auth.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    BackendModule.forRoot('http://localhost:3000'),
    AuthModule.forClientRoot({
      serverUrl: `http://localhost:4200/authentication`,
    }),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
