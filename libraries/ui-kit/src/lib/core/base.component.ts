import { Directive, HostBinding, Input, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';

@Directive()
export class BaseComponent implements OnDestroy {
  @HostBinding('attr.display-contents')
  displayContents = true;

  @Input('class')
  classes?: string | null;

  private _destroySubject = new Subject();
  protected _destroyed = this._destroySubject.asObservable();

  ngOnDestroy(): void {
    this._destroySubject.next();
    this._destroySubject.complete();
  }
}
