import { Component, OnInit } from '@angular/core';
import { TokenApiService } from '@kazgis/backend/services/token.api-service';
import { BaseComponent } from '@kazgis/ui-kit/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [TokenApiService],
})
export class AppComponent extends BaseComponent implements OnInit {
  constructor(private tokenApi: TokenApiService) {
    super();
  }

  title = 'admin';

  ngOnInit(): void {}
}
