import { Component } from '@angular/core';
import { BaseComponent } from '@kazgis/ui-kit/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent extends BaseComponent {
  title = 'identity';
}
