import {ModuleWithProviders, NgModule} from "@angular/core";
import {HttpClientModule} from "@angular/common/http";
import {BASE_URL} from "./tokens";

@NgModule({
})
export class BackendModule {
  static forRoot(baseUrl: string): ModuleWithProviders<BackendModuleForRoot> {
    return {
      ngModule: BackendModuleForRoot,
      providers: [
        {provide: BASE_URL, useValue: baseUrl}
      ]
    }
  }
}

@NgModule({
  imports: [HttpClientModule, BackendModule],
  exports: [BackendModule]
})
export class BackendModuleForRoot {

}
