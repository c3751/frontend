import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-identity-not-authenticated',
  templateUrl: './identity-not-authenticated.component.html',
  styleUrls: ['./identity-not-authenticated.component.scss'],
})
export class IdentityNotAuthenticatedComponent implements OnInit {
  constructor(private _router: Router) {}

  async ngOnInit() {
    await this._router.navigate(['/authentication']);
  }
}
