import { SynchronizationConfiguration, Synchronize } from '@dev-stream/utils';
import { Injectable } from '@angular/core';
import { TokenVM } from '@kazgis/backend/models/ITokenVm';
import * as moment from 'moment';
import { BaseAuthService } from '@kazgis/core/auth/base-auth.service';

export const SYNC_CONFIG = new SynchronizationConfiguration({
  storage: sessionStorage,
  prefix: 'KAZGIS.core.AuthClientService',
});

@Injectable()
export class AuthClientService extends BaseAuthService {
  @Synchronize(SYNC_CONFIG)
  public tokenInfo?: TokenVM;

  public isAuthenticated() {
    return (
      this.tokenInfo != null &&
      this.tokenInfo.token != null &&
      moment(this.tokenInfo.expires).isAfter(moment())
    );
  }

  public logout() {
    SYNC_CONFIG.clear();
  }
}
