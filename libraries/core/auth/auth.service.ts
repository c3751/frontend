import { Injectable } from '@angular/core';
import { TokenApiService } from '@kazgis/backend/services/token.api-service';
import { mapLeft, mapRight } from '@kazgis/utils/rx/eitherify';
import { UserNotFoundError } from '@kazgis/core/errors/user-not-found.error';
import { GeneralError } from '@kazgis/core/errors/general.error';
import { SynchronizationConfiguration, Synchronize } from '@dev-stream/utils';
import { TokenVM } from '@kazgis/backend/models/ITokenVm';
import * as moment from 'moment';
import { BaseAuthService } from '@kazgis/core/auth/base-auth.service';

export const SYNC_CONFIG = new SynchronizationConfiguration({
  storage: localStorage,
  prefix: 'KAZGIS.core.AuthService',
});

@Injectable()
export class AuthService extends BaseAuthService {
  @Synchronize(SYNC_CONFIG)
  public tokenInfo?: TokenVM;

  public isAuthenticated() {
    return (
      this.tokenInfo != null &&
      this.tokenInfo.token != null &&
      moment(this.tokenInfo.expires).isAfter(moment())
    );
  }

  constructor(protected _tokenApi: TokenApiService) {
    super();
  }

  public authenticate(username: string, password: string) {
    return this._tokenApi.token(username, password).pipe(
      mapLeft((error) => {
        if (error.status == 404) {
          return new UserNotFoundError();
        }
        return new GeneralError();
      }),
      mapRight((response) => {
        this.tokenInfo = response;
        return response;
      })
    );
  }

  public logout() {
    SYNC_CONFIG.clear();
  }
}
