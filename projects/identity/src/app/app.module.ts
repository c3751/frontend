import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BackendModule } from '@kazgis/backend/backend.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthModule } from '@kazgis/core/auth/auth.module';
import { IdentityNotAuthenticatedComponent } from './components/identity-not-authenticated/identity-not-authenticated.component';

@NgModule({
  declarations: [AppComponent, IdentityNotAuthenticatedComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    BackendModule.forRoot('http://localhost:3000'),
    AuthModule.forServerRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
