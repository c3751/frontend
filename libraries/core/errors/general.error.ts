export class GeneralError extends Error {
  constructor() {
    super('Unknown error');
    Object.setPrototypeOf(this, GeneralError.prototype);
  }
}
