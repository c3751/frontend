import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BaseComponent } from '@kazgis/ui-kit/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'ui-sign-in-form',
  templateUrl: './sign-in-form.component.html',
  styleUrls: ['./sign-in-form.component.css'],
})
export class SignInFormComponent extends BaseComponent implements OnInit {
  @Input()
  usernameControl: FormControl = new FormControl('', [Validators.required]);

  @Input()
  passwordControl: FormControl = new FormControl('', [Validators.required]);

  @Output()
  formFilled = new EventEmitter<[string, string]>();

  constructor() {
    super();
  }

  ngOnInit(): void {}

  submit() {
    console.log(this.passwordControl.valid && this.usernameControl.valid);
    if (this.passwordControl.valid && this.usernameControl.valid) {
      this.formFilled.emit([
        this.usernameControl.value,
        this.passwordControl.value,
      ]);
    }
  }
}
