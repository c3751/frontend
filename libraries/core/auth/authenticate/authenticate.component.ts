import { Component, OnInit } from '@angular/core';
import { BaseAuthService } from '@kazgis/core/auth/base-auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TokenVM } from '@kazgis/backend/models/ITokenVm';

@Component({
  selector: 'app-authenticate',
  templateUrl: './authenticate.component.html',
  styleUrls: ['./authenticate.component.scss'],
})
export class AuthenticateComponent implements OnInit {
  constructor(
    private _auth: BaseAuthService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute
  ) {}

  async ngOnInit() {
    const hasToken = this._activatedRoute.snapshot.queryParamMap.has('token');

    if (!hasToken) {
      await this._router.navigate(['/error']);
      return;
    }

    try {
      const token = JSON.parse(
        this._activatedRoute.snapshot.queryParamMap.get('token')!
      );
      this._auth.tokenInfo = new TokenVM(token);

      // TODO redirect to previous page

      await this._router.navigate(['/']);
    } catch (ex) {
      await this._router.navigate(['/error']);
    }
  }
}
