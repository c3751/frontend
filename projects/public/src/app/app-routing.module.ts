import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotAuthenticatedComponent } from '@kazgis/core/auth/not-authenticated/not-authenticated.component';
import { AuthenticateComponent } from '@kazgis/core/auth/authenticate/authenticate.component';
import { AuthGuard } from '@kazgis/core/auth/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'test',
    pathMatch: 'full',
  },
  {
    path: 'not-authenticated',
    component: NotAuthenticatedComponent,
  },
  {
    path: 'authenticate',
    component: AuthenticateComponent,
  },
  {
    path: 'test',
    children: [],
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
