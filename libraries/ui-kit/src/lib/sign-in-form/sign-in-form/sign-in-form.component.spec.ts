import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SignInFormComponent } from './sign-in-form.component';
import { first } from 'rxjs/operators';

describe('SignInFormComponent', () => {
  let component: SignInFormComponent;
  let fixture: ComponentFixture<SignInFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SignInFormComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SignInFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit event on click', () => {
    component.formFilled.pipe(first()).subscribe((form) => {
      expect(form).toEqual(['admin', 'admin']);
    });
    component.usernameControl.setValue('admin');
    component.passwordControl.setValue('admin');
    // component.formFilled.emit();
    // expect(3).toEqual(4);
    // component.submit();
    // component.submit();
    // fixture.detectChanges();
  });
});
