import { Component, Inject, OnInit } from '@angular/core';
import { AUTH_SERVER_URL } from '@kazgis/core/auth/tokens';

@Component({
  selector: 'app-not-authenticated',
  templateUrl: './not-authenticated.component.html',
  styleUrls: ['./not-authenticated.component.scss'],
})
export class NotAuthenticatedComponent implements OnInit {
  constructor(@Inject(AUTH_SERVER_URL) private _authServerUrl: string) {}

  ngOnInit(): void {
    window.location.assign(
      `${this._authServerUrl}?redirectTo=${window.location.protocol}//${window.location.host}`
    );
  }
}
