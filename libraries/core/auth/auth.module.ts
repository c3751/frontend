import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from '@kazgis/core/auth/auth.interceptor';
import { AuthGuard } from '@kazgis/core/auth/auth.guard';
import { BaseAuthService } from '@kazgis/core/auth/base-auth.service';
import { AuthClientService } from '@kazgis/core/auth/auth-client.service';
import { AuthService } from '@kazgis/core/auth/auth.service';
import { TokenApiService } from '@kazgis/backend/services/token.api-service';
import { NotAuthenticatedComponent } from './not-authenticated/not-authenticated.component';
import { AUTH_SERVER_URL } from '@kazgis/core/auth/tokens';
import { AuthenticateComponent } from './authenticate/authenticate.component';

@NgModule({
  declarations: [NotAuthenticatedComponent, AuthenticateComponent],
  imports: [CommonModule],
})
export class AuthModule {
  static forServerRoot(): ModuleWithProviders<AuthRootModule> {
    return {
      ngModule: AuthRootModule,
      providers: [
        { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
        {
          provide: BaseAuthService,
          useClass: AuthService,
          deps: [TokenApiService],
        },
        AuthGuard,
      ],
    };
  }

  static forClientRoot(config: {
    serverUrl: string;
  }): ModuleWithProviders<AuthRootModule> {
    return {
      ngModule: AuthRootModule,
      providers: [
        { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
        { provide: BaseAuthService, useClass: AuthClientService },
        { provide: AUTH_SERVER_URL, useValue: config.serverUrl },
        AuthGuard,
      ],
    };
  }
}

@NgModule({
  imports: [AuthModule],
  exports: [AuthModule],
})
export class AuthRootModule {}
