import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BusinessFormsMainPageComponent } from './business-forms-main-page/business-forms-main-page.component';

const routes: Routes = [
  {
    path: '',
    component: BusinessFormsMainPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BusinessFormsRoutingModule {}
