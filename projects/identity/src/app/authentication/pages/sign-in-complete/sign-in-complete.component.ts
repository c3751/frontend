import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@kazgis/ui-kit/core';

@Component({
  selector: 'app-sign-in-complete',
  templateUrl: './sign-in-complete.component.html',
  styleUrls: ['./sign-in-complete.component.scss'],
})
export class SignInCompleteComponent extends BaseComponent implements OnInit {
  constructor() {
    super();
  }

  ngOnInit(): void {}
}
