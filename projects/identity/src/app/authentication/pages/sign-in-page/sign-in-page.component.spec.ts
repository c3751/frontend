import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SignInPageComponent } from './sign-in-page.component';
import { BackendModule } from '@kazgis/backend/backend.module';
import { AuthModule } from '@kazgis/core/auth/auth.module';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { SignInFormModule } from '@kazgis/ui-kit/sign-in-form';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TokenVM } from '@kazgis/backend/models/ITokenVm';
import { HttpErrorResponse } from '@angular/common/http';
import { UserNotFoundError } from '@kazgis/core/errors/user-not-found.error';
import { GeneralError } from '@kazgis/core/errors/general.error';

describe('SignInPageComponent', () => {
  let component: SignInPageComponent;
  let fixture: ComponentFixture<SignInPageComponent>;
  let httpTestingController: HttpTestingController;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        BrowserAnimationsModule,
        SignInFormModule,
        BackendModule.forRoot('http://localhost:3000'),
        AuthModule.forServerRoot(),
      ],
      declarations: [SignInPageComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SignInPageComponent);
    component = fixture.componentInstance;
    httpTestingController = TestBed.inject(HttpTestingController);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should make http call', () => {
    component.submit(['admin', 'admin']).subscribe((result) => {
      expect(result.value).toBeInstanceOf(Promise);
    });
    const req = httpTestingController.expectOne(`http://localhost:3000/token`);

    const response = new TokenVM({
      token: '',
      expires: new Date(),
      id: '',
      isAdmin: true,
    });

    req.flush(response);

    fixture.detectChanges();
    // expect(result.value).toBeInstanceOf(Promise);
  });

  it('should make http call and have error branch', () => {
    component.submit(['admin', 'admin']).subscribe((result) => {
      expect(result.value).toBeInstanceOf(UserNotFoundError);
    });
    const req = httpTestingController.expectOne(`http://localhost:3000/token`);

    const response = new HttpErrorResponse({
      status: 404,
    });

    req.flush(response, { status: 404, statusText: 'NotFound' });

    fixture.detectChanges();
    // expect(result.value).toBeInstanceOf(Promise);
  });

  it('should make http call and have general error', () => {
    component.submit(['admin', 'admin']).subscribe((result) => {
      expect(result.value).toBeInstanceOf(GeneralError);
    });
    const req = httpTestingController.expectOne(`http://localhost:3000/token`);

    req.flush("", { status: 500, statusText: 'ServerError' });

    fixture.detectChanges();
    // expect(result.value).toBeInstanceOf(Promise);
  });
});
