import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotAuthenticatedComponent } from '@kazgis/core/auth/not-authenticated/not-authenticated.component';
import { AuthGuard } from '@kazgis/core/auth/auth.guard';
import { AuthenticateComponent } from '@kazgis/core/auth/authenticate/authenticate.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'forms',
    pathMatch: 'full',
  },
  {
    path: 'not-authenticated',
    component: NotAuthenticatedComponent,
  },
  {
    path: 'authenticate',
    component: AuthenticateComponent,
  },
  {
    path: 'forms',
    loadChildren: () =>
      import('./pages/business-forms/business-forms.module').then(
        (m) => m.BusinessFormsModule
      ),
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
