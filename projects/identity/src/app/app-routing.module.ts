import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IdentityNotAuthenticatedComponent } from './components/identity-not-authenticated/identity-not-authenticated.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'authentication',
    pathMatch: 'full',
  },
  {
    path: 'not-authenticated',
    component: IdentityNotAuthenticatedComponent,
  },
  {
    path: 'authentication',
    loadChildren: () =>
      import('./authentication/authentication.module').then(
        (m) => m.AuthenticationModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
