import {
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseAuthService } from '@kazgis/core/auth/base-auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private _auth: BaseAuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    if (req.headers.get('skip-auth') == 'true') {
      return next.handle(req);
    }

    if (this._auth.isAuthenticated()) {
      return next.handle(
        req.clone({
          setHeaders: {
            auth: this._auth.tokenInfo!.token,
          },
        })
      );
    } else {
      return next.handle(req);
    }
  }
}
