import {InjectionToken} from "@angular/core";

export const BASE_URL = new InjectionToken<string>('kazgis.lib.backend.base-url');
