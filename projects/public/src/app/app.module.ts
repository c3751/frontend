import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthModule } from '@kazgis/core/auth/auth.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AuthModule.forClientRoot({
      serverUrl: `http://localhost:4200/authentication`,
    }),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
