import { Either, left, right } from '@sweet-monads/either';
import { of, OperatorFunction } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';

export function eitherify<TSource, TError extends Error>(): OperatorFunction<
  TSource,
  Either<HttpErrorResponse, TSource>
> {
  return (source) =>
    source.pipe(
      map((sourceRes) => right(sourceRes)),
      catchError((exception) => of(left(exception)))
    );
}

export type EitherLeft<TEither> = TEither extends Either<infer TLeft, any>
  ? TLeft
  : never;
export type EitherRight<TEither> = TEither extends Either<any, infer TRight>
  ? TRight
  : never;

export function mapLeft<TSource extends Either<any, any>, TResult>(
  mapper: (source: EitherLeft<TSource>) => TResult
): OperatorFunction<TSource, Either<TResult, EitherRight<TSource>>> {
  return (source) => source.pipe(map((either) => either.mapLeft(mapper)));
}

export function mapRight<TSource extends Either<any, any>, TResult>(
  mapper: (source: EitherRight<TSource>) => TResult
): OperatorFunction<TSource, Either<EitherLeft<TSource>, TResult>> {
  return (source) => source.pipe(map((either) => either.mapRight(mapper)));
}
