export interface ITokenVm {
  id: string;
  expires: string;
  isAdmin: boolean;
  token: string;
}

export class TokenVM {
  id!: string;
  expires!: Date;
  isAdmin!: boolean;
  token!: string;

  constructor(...parts: Partial<TokenVM>[]) {
    Object.assign(this, ...parts);

    if (this.expires) {
      this.expires = new Date(this.expires);
    }
  }
}
