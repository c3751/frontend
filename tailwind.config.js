const genSpacing = () => {
  const spacing = {
    auto: "auto",
  };
  for (let i = 0; i <= 100; i++) {
    spacing[i * 2] = i * 2 + "px";
    spacing[-i * 2] = i * 2 + "px";
  }
  return spacing;
};

const spacing = {
  spacing: genSpacing(),
  borderRadius: {
    ...genSpacing(),
    none: "0",
    full: "100%",
  },
  minWidth: {
    ...genSpacing(),
    min: "min-content",
    max: "max-content",
  },
  maxWidth: {
    ...genSpacing(),
    min: "min-content",
    max: "max-content",
  },
  minHeight: {
    ...genSpacing(),
    min: "min-content",
    max: "max-content",
  },
  maxHeight: {
    ...genSpacing(),
    min: "min-content",
    max: "max-content",
  },
};

module.exports = {
  mode: "jit",
  purge: {
    content: ["applications/**/*", "libraries/**/*"],
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
    spacing: spacing.spacing,
    minWidth: spacing.minWidth,
    maxWidth: spacing.maxWidth,
    minHeight: spacing.minHeight,
    maxHeight: spacing.maxHeight,
    borderRadius: spacing.borderRadius,
  },
  variants: {
    extend: {
      opacity: ["disabled"],
      textOpacity: ["disabled"],
      backgroundOpacity: ["disabled"],
      backgroundColor: ["disabled", "checked"],
      textColor: ["disabled"],
      borderColor: ["disabled", "checked"],
      boxShadow: ["disabled"],
      cursor: ["disabled"],
    },
  },
  plugins: [],
};
