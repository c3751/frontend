import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessFormsMainPageComponent } from './business-forms-main-page.component';

describe('BusinessFormsMainPageComponent', () => {
  let component: BusinessFormsMainPageComponent;
  let fixture: ComponentFixture<BusinessFormsMainPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BusinessFormsMainPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessFormsMainPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
