import { InjectionToken } from '@angular/core';

export const AUTH_SERVER_URL = new InjectionToken(
  'kazgis.auth.auth-server-url'
);
