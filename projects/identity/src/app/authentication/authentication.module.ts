import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthenticationRoutingModule } from './authentication-routing.module';
import { SignInPageComponent } from './pages/sign-in-page/sign-in-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SignInFormModule } from '@kazgis/ui-kit/sign-in-form';
import { SignInCompleteComponent } from './pages/sign-in-complete/sign-in-complete.component';

@NgModule({
  declarations: [SignInPageComponent, SignInCompleteComponent],
  imports: [
    CommonModule,
    FormsModule,
    SignInFormModule,
    ReactiveFormsModule,
    AuthenticationRoutingModule,
  ],
})
export class AuthenticationModule {}
