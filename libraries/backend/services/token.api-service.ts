import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { BASE_URL } from '../tokens';
import { Either, left, right } from '@sweet-monads/either';
import { TokenVM } from '@kazgis/backend/models/ITokenVm';
import { eitherify, mapRight } from '@kazgis/utils/rx/eitherify';

@Injectable({ providedIn: 'root' })
export class TokenApiService {
  constructor(
    private _http: HttpClient,
    @Inject(BASE_URL) private baseUrl: string
  ) {}

  public token(username: string, password: string) {
    return this._http
      .post<TokenVM>(`${this.baseUrl}/token`, {
        username,
        password,
      })
      .pipe(
        eitherify(),
        mapRight((response) => new TokenVM(response))
      );
  }
}
