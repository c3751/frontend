import { Component, Inject, OnInit } from '@angular/core';
import { BaseComponent } from '@kazgis/ui-kit/core';
import { AuthService } from '@kazgis/core/auth/auth.service';
import { share, takeUntil } from 'rxjs/operators';
import { mapLeft, mapRight } from '@kazgis/utils/rx/eitherify';
import { UserNotFoundError } from '@kazgis/core/errors/user-not-found.error';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseAuthService } from '@kazgis/core/auth/base-auth.service';
import { TokenVM } from '@kazgis/backend/models/ITokenVm';

@Component({
  selector: 'app-sign-in-page',
  templateUrl: './sign-in-page.component.html',
  styleUrls: ['./sign-in-page.component.scss'],
})
export class SignInPageComponent extends BaseComponent implements OnInit {
  constructor(
    @Inject(BaseAuthService) private _auth: AuthService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute
  ) {
    super();
  }

  private async handleAuth(token: TokenVM) {
    const isRedirect =
      this._activatedRoute.snapshot.queryParamMap.has('redirectTo');
    if (isRedirect) {
      this.externalAuthHandle(token);
    } else {
      await this.internalAuthHandle(token);
    }
    return true;
  }

  async ngOnInit() {
    // if (this._auth.isAuthenticated()) {
    //   await this.handleAuth(this._auth.tokenInfo!);
    // }
  }

  submit(event: [string, string]) {
    const [username, password] = event;
    const obs = this._auth.authenticate(username, password).pipe(
      takeUntil(this._destroyed),
      mapRight(this.handleAuth.bind(this)),
      mapLeft((error) => {
        if (error instanceof UserNotFoundError) {
          // alert('Wrong username or password');
        } else {
          // alert('Unknown error, please try again later');
        }
        return error;
      }),
      share()
    );

    obs.subscribe();
    return obs;
  }

  private externalAuthHandle(response: TokenVM) {
    const redirectHost =
      this._activatedRoute.snapshot.queryParamMap.get('redirectTo');
    // window.location.assign(
    //   `${redirectHost}/authenticate?token=${JSON.stringify(response)}`
    // );
  }

  private async internalAuthHandle(response: TokenVM) {
    // await this._router.navigate(['./complete'], {
    //   relativeTo: this._activatedRoute,
    // });
  }
}
