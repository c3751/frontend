import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignInPageComponent } from './pages/sign-in-page/sign-in-page.component';
import { SignInCompleteComponent } from './pages/sign-in-complete/sign-in-complete.component';
import { AuthGuard } from '@kazgis/core/auth/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: SignInPageComponent,
  },
  {
    path: 'complete',
    component: SignInCompleteComponent,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthenticationRoutingModule {}
