import { Synchronize } from '@dev-stream/utils';
import { TokenVM } from '@kazgis/backend/models/ITokenVm';
import { SYNC_CONFIG } from '@kazgis/core/auth/auth-client.service';

export abstract class BaseAuthService {
  public tokenInfo?: TokenVM;
  public abstract isAuthenticated(): boolean;
  public abstract logout(): any;
}
