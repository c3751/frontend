import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanLoad,
  Route,
  Router,
  RouterStateSnapshot,
  UrlSegment,
} from '@angular/router';
import { Injectable } from '@angular/core';
import { BaseAuthService } from '@kazgis/core/auth/base-auth.service';

@Injectable()
export class AuthGuard implements CanActivate, CanLoad {
  constructor(private _auth: BaseAuthService, private _router: Router) {}

  async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this._auth.isAuthenticated()) {
      return true;
    }
    return await this._router.navigate(['/not-authenticated']);
  }

  async canLoad(route: Route, segments: UrlSegment[]) {
    if (this._auth.isAuthenticated()) {
      return true;
    }
    return await this._router.navigate(['/not-authenticated']);
  }
}
